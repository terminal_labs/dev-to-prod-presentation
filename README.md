# README #

### What Is This Repository For? ###
This repo is for automatic provisioning of a python flask app in a virtual machine (either on virtualbox or on digitalocean). The virtual machine also will use nginx, uwsgi, postgres, and supervisor.

It is from a presentation I gave (on March 23 2017) at the Capital Factory in Austin TX on "How to take a flask app that you can run locally on your laptop and move it to virtual machines on DigitalOcean and support hundreds of users."

### Hardware Recommendations ###
*  reasonably fast cpu with 2 or more cores and vt-x (I used a Intel i7-3612QM 2.1GHz, 4 core chip)
*  8gb ram (or more)
*  16gb free  drive space

### Summary of Setup: ###


### Dependencies: ###
[Xubuntu 16.04 or newer](https://xubuntu.org/news/xubuntu-16-04-release/)

[Vagrant 1.9 or newer](http://www.vagrantup.com/)

[VirtualBox 5.1 or newer](https://www.virtualbox.org/)

### Deployment Instructions: ###
*   setup your bare-metal host with Xubuntu (or newer or similar distributions. also a mac works)
*   install build-essential and linux-headers on your host with apt (or apt-get)
*   install all the listed dependencies on you host with the debian package manager

### Usage Instructions: ###
*   clone the repo (or download it)
*   cd into dev_to_prod_presentation/vagrant_local
*   run "vagrant up"


