from locust import HttpLocust, TaskSet

def login(l):
    l.client.get("/new-category")

def index(l):
    l.client.get("/new-task")

def profile(l):
    l.client.get("/")

class UserBehavior(TaskSet):
    tasks = {login: 1, index: 1, profile: 1}

    #def on_start(self):
    #    login(self)

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 250
    max_wait = 500
