cd /vagrant/deployment/nginx
apt install --yes nginx-full
rm /etc/nginx/sites-enabled/default
cp nginx.conf /etc/nginx/nginx.conf
cp uwsgi_app.conf /etc/nginx/sites-enabled/uwsgi_app.conf
su uwsgi_app_user -c "cp uwsgi_params.txt /home/uwsgi_app_user/uwsgi_params.txt"
service nginx restart
