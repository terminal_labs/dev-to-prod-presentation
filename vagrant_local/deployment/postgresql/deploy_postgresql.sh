cd /vagrant/deployment/postgresql
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
apt install --yes postgresql-9.4
apt install --yes libpq-dev
