cd /vagrant/deployment/app
cd /home/uwsgi_app_user
su postgres -c "psql -c \"create user patrycja with password 'mypassword';\"" #not secure
su postgres -c "psql -c \"create database todoapp owner patrycja encoding 'utf-8';\""
su uwsgi_app_user -c "git clone https://github.com/pdybka-ep/flask-todoapp.git app_repo"
su uwsgi_app_user -c "virtualenv venv"
su uwsgi_app_user -c "venv/bin/pip install uwsgi"
su uwsgi_app_user -c "venv/bin/pip install -r app_repo/requirements.txt"
su uwsgi_app_user -c "venv/bin/python app_repo/manage.py init_db"
cd -
su uwsgi_app_user -c "cp wsgi.py /home/uwsgi_app_user/app_repo/wsgi.py"
