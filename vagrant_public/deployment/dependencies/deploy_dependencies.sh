cd /vagrant/deployment/dependencies
apt update --yes -q
DEBIAN_FRONTEND=noninteractive apt -y -q -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" full-upgrade
apt install --yes linux-headers-$(uname -r)
apt install --yes build-essential
apt install --yes python-setuptools
apt install --yes python-virtualenv
apt install --yes python-pip
apt install --yes python-dev
apt install --yes git
apt install --yes emacs
