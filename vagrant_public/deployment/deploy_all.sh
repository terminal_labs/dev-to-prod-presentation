cd /vagrant/deployment
sudo bash users/deploy_users.sh
sudo bash dependencies/deploy_dependencies.sh
sudo bash postgresql/deploy_postgresql.sh
sudo bash nginx/deploy_nginx.sh
sudo bash app/deploy_app.sh
sudo bash supervisord/deploy_supervisord.sh
